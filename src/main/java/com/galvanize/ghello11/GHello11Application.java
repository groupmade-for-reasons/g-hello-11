package com.galvanize.ghello11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GHello11Application {

	public static void main(String[] args) {
		SpringApplication.run(GHello11Application.class, args);
	}

}